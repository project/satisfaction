<?php

/**
 * @file
 * Administration pages and forms for the satisfaction module.
 */

/**
 * Form builder; Satisfaction configuration page callback.
 */
function satisfaction_settings() {
  $form = array();

  // Community settings.

  $form['community'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get Satisfaction community settings'),
  );

  $form['community']['satisfaction_company_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Company ID'),
    '#description' => t('Insert a valid Get Satisfaction community ID. This ID is used to filter topics on autocomplete fields and to which topics created in the portal will be added to.'),
    '#default_value' => variable_get('satisfaction_company_id', FALSE),
    '#required' => TRUE,
  );

  // Entity type configuration.

  $form['entities'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get Satisfaction enabled entities'),
    '#description' => t('Pick the entity/bundle pairs that can take advantage of Get Satisfaction integration.'),
  );

  $entities = entity_get_info();

  foreach ($entities as $entity_key => $entity) {
    if (!$entity['fieldable']) {
      continue;
    }

    $form['entities'][$entity_key] = array(
      '#type' => 'fieldset',
      '#title' => t($entity['label']),
    );

    foreach ($entity['bundles'] as $bundle_key => $bundle) {
      $field_key = 'satisfaction_entity_' . $entity_key . '_' . $bundle_key;

      $form['entities'][$entity_key][$field_key] = array(
        '#type' => 'checkbox',
        '#title' => t($bundle['label']),
        '#default_value' => variable_get($field_key, FALSE),
      );
    }
  }

  $form['#validate'] = array(
    'satisfaction_settings_validate'
  );

  return system_settings_form($form);
}

/**
 * Satisfaction settings form validation callback.
 *
 * Validates the consumer key and secret against Get Satisfaction API.
 */
function satisfaction_settings_validate($form, $form_state) {

  if (form_get_errors()) {
    return;
  }

  // @todo validate company id.
}
