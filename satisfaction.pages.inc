<?php

/**
 * @file
 * Non-administration page callbacks.
 */

/**
 * Menu callback; returns a list of Topics matching a given string.
 */
function satisfaction_autocomplete_topics($string = '') {
  $matches = array();

  $client = new SatisfactionClient();

  $company_id = variable_get('satisfaction_company_id', '');

  $topics = $client->getCompanyTopics($company_id, array('q' => check_plain($string)));

  foreach ($topics->data as $topic) {
    $key = satisfaction_key_string_compose($topic->subject, $topic->id);
    $matches[$key] = $topic->subject;
  }

  drupal_json_output($matches);
}
