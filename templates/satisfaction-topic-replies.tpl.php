<?php

/**
 * @file
 * Get Satisfaction topic replies template.
 *
 * Available variables:
 * - $replies: An array of replies that exist inside the topic.
 *
 * @todo Add translation, and improve template.
 */

?>

<h2>Replies in Get Satisfaction</h2>

<?php foreach ($replies as $reply): ?>
  <article>
    <div><?php print filter_xss($reply->content, array('img')); ?></div>
    <p>
      <img src="<?php print filter_xss($reply->author->avatar_url_small); ?>">
      <span><?php print check_plain($reply->author->name); ?></span>
      <span><?php print check_plain($reply->created_at); ?></span>
    </p>
  </article>
<?php endforeach; ?>

<?php if (empty($replies)): ?>
  <p>No replies yet. Add yours?</p>
<?php endif; ?>
