<?php

/**
 * @file
 * Holds theme functions declared in satisfaction module.
 */

/**
 * Preprocesses variables for theme_satisfaction_topic_replies().
 */
function template_preprocess_satisfaction_topic_replies(&$vars) {
  $vars['replies'] = array();

  $client = new SatisfactionClient();

  if (!empty($vars['topic_id'])) {
    $replies = $client->getTopicReplies($vars['topic_id']);

    if (!empty($replies)) {
      $vars['replies'] = $replies->data;
    }
  }
}
