<?php

/**
 * @file
 * Holds SatisfactionClient class.
 */

/**
 * The central facade class that interfaces with Get Satisfaction API.
 */
class SatisfactionClient {

  private $apiUrl = 'https://api.getsatisfaction.com';

  public function postTopic($topic) {
    return $this->authenticatedRequest('/topics', array(), array('topic' => $topic), 'POST');
  }

  public function getTopics($filters = array()) {
    return $this->anonymousRequest('/topics', $filters);
  }

  public function getTopic($topicId) {
    return $this->anonymousRequest('/topics/' . $topicId);
  }

  public function getTopicReplies($topicId, $filters = array()) {
    return $this->anonymousRequest('/topics/' . $topicId . '/replies', $filters);
  }

  public function getCompanyTopics($companyId, $filters = array()) {
    return $this->anonymousRequest('/companies/' . $companyId . '/topics', $filters);
  }

  private function anonymousRequest($endpoint, $query = array(), $data = array(), $method = 'GET') {

    $requestUrl = $this->apiUrl . $endpoint . '.json?' . drupal_http_build_query($query);

    $response = drupal_http_request($requestUrl, array(
      'method' => $method,
      'data' => '',
    ));

    $responseData = json_decode($response->data);

    if ($response->code == '200') {
      return $responseData;
    }

    return array();
  }

  private function authenticatedRequest($endpoint, $query = array(), $data = array(), $method = 'GET') {
    $requestUrl = $this->apiUrl . $endpoint;

    $params = $query;
    $params['post_data'] = $data;

    $request = oauthconnector_endpoint_call_for_user($requestUrl, $params, 'get_satisfaction', NULL, $method);

    return $request;
  }

}
