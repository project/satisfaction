<?php

/**
 * @file
 * The main file for satisfaction module.
 */

/**
 * Implements hook_menu().
 */
function satisfaction_menu() {
  $items = array();

  $items['admin/config/services/satisfaction'] = array(
    'title' => 'Get Satisfaction',
    'description' => 'Configure Get Satisfaction API access and behavior.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('satisfaction_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'satisfaction.admin.inc',
  );

  $items['satisfaction/autocomplete/topics'] = array(
    'title' => 'Topics Autocomplete',
    'page callback' => 'satisfaction_autocomplete_topics',
    'access callback' => 'user_is_logged_in',
    'file' => 'satisfaction.pages.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function satisfaction_theme($existing, $type, $theme, $path) {
  $themes = array();

  $themes['satisfaction_topic_replies'] = array(
    'variables' => array(
      'topic_id' => NULL,
    ),
    'template' => 'templates/satisfaction-topic-replies',
    'file' => 'satisfaction.theme.inc',
  );

  return $themes;
}

/**
 * Implements hook_field_extra_fields().
 */
function satisfaction_field_extra_fields() {
  $extra = array();

  $entities = satisfaction_get_enabled_entities();

  foreach ($entities as $entity_key => $bundles) {
    foreach ($bundles as $bundle_key) {
      $extra[$entity_key][$bundle_key] = array(
        'form' => array(
          'satisfaction' => array(
            'label' => 'Get Satisfaction',
            'description' => '',
            'weight' => 0,
          ),
        ),
        'display' => array(
          'satisfaction' => array(
            'label' => 'Get Satisfaction',
            'description' => '',
            'weight' => 0,
          ),
        ),
      );
    }
  }

  return $extra;
}

/**
 * Implements hook_field_attach_form().
 *
 * Attaches the extra fields declared in satisfaction_field_extra_fields() to
 * the entity form for the entity/bundle pairs that have it enabled.
 */
function satisfaction_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {

  if (!satisfaction_is_enabled_entity($entity, $entity_type)) {
    return;
  }

  $form['satisfaction'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get Satisfaction'),
    '#tree' => TRUE,
  );

  $form['satisfaction']['op'] = array(
    '#type' => 'radios',
    '#title' => t('Operation'),
    '#access' => satisfaction_is_user_connected(),
    '#options' => array(
      'link' => t('Link to an existing topic'),
      'new' => t('Create a new topic'),
    ),
    '#default_value' => 'link',
  );

  $form['satisfaction']['topic'] = array(
    '#type' => 'textfield',
    '#title' => t('Linked topic'),
    '#autocomplete_path' => 'satisfaction/autocomplete/topics',
    '#default_value' => empty($entity->satisfaction_state['topic']->title) ? '' : $entity->satisfaction_state['topic']->title,
    '#states' => array(
      'visible' => array(
        ':input[name="satisfaction[op]"]' => array('value' => 'link'),
      ),
    ),
  );

  $form['satisfaction']['new'] = array(
    '#type' => 'container',
    '#access' => satisfaction_is_user_connected(),
    '#states' => array(
      'visible' => array(
        ':input[name="satisfaction[op]"]' => array('value' => 'new'),
      ),
    ),
  );

  $form['satisfaction']['new']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Topic Title'),
  );

  $form['satisfaction']['new']['additional_detail'] = array(
    '#type' => 'textarea',
    '#title' => t('Topic Details'),
  );

  $form['satisfaction']['new']['style'] = array(
    '#type' => 'select',
    '#title' => t('Topic Style'),
    '#options' => array(
      'question' => t('Question'),
      'problem' => t('Problem'),
      'praise' => t('Praise'),
      'idea' => t('Idea'),
      'update' => t('Update')
    ),
  );

  $form['satisfaction']['new']['keywords'] = array(
    '#type' => 'textfield',
    '#title' => t('Topic Keywords'),
    '#attributes' => array(
      'placeholder' => t('Separate keywords with a comma, for example: Fancy Product, Awesome Company, Happy Client'),
    ),
  );
}

/**
 * Implements hook_entity_insert().
 */
function satisfaction_entity_insert($entity, $type) {
  satisfaction_entity_update($entity, $type);
}

/**
 * Implements hook_entity_update().
 */
function satisfaction_entity_update($entity, $entity_type) {
  if (empty($entity->satisfaction)) {
    return;
  }

  $client = new SatisfactionClient();

  $entity_info = entity_extract_ids($entity_type, $entity);
  $entity_id = $entity_info[0];
  $entity_bundle = $entity_info[2];

  // If the input new is set, create a new topic and override the topic field.
  if (!empty($entity->satisfaction['new']['title'])) {

    // Create a new topic based on the input data.
    $topic = new stdClass();
    $topic->subject = $entity->satisfaction['new']['title'];
    $topic->style = $entity->satisfaction['new']['style'];
    $topic->keywords = $entity->satisfaction['new']['keywords'];
    $topic->additional_detail = $entity->satisfaction['new']['additional_detail'];
    $topic->company_domain = variable_get('satisfaction_company_id', '');

    // Post it to Get Satisfaction and overwrite the topic input.
    $request = $client->postTopic($topic);

    if (!empty($request['subject']) && !empty($request['id'])) {
      $entity->satisfaction['topic'] = satisfaction_key_string_compose($request['subject'], $request['id']);
    }
  }

  // Insert or update the topic relationship if the topic input is set.
  if (!empty($entity->satisfaction['topic'])) {
    $topic_key = satisfaction_key_string_decompose($entity->satisfaction['topic']);

    if (!empty($topic_key)) {
      db_merge('satisfaction_entity_topics')
        ->key(array('entity_type', 'entity_id'), array($entity_type, $entity_id))
        ->fields(array('topic_id', 'title'), array($topic_key['id'], $entity->satisfaction['topic']))
        ->execute();
    }
  }

  // Remove a topic relationship if it exists and the input is empty.
  if (empty($entity->satisfaction['topic'])) {
    db_delete('satisfaction_entity_topics')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->execute();
  }
}

/**
 * Implements hook_entity_load().
 *
 * Inject data into entities with satisfaction integration enabled.
 */
function satisfaction_entity_load($entities, $entity_type) {
  foreach ($entities as $entity) {
    if (!satisfaction_is_enabled_entity($entity, $entity_type)) {
      return;
    }

    $entity_info = entity_extract_ids($entity_type, $entity);
    $entity_id = $entity_info[0];

    $result = db_select('satisfaction_entity_topics', 't')
      ->fields('t')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->execute()
      ->fetch();

    $entity->satisfaction_state = array(
      'topic' => $result
    );
  }
}

/**
 * Implements hook_entity_view().
 */
function satisfaction_entity_view($entity, $entity_type, $view_mode, $langcode) {
  if (!satisfaction_is_enabled_entity($entity, $entity_type)) {
    return;
  }

  if (!empty($entity->satisfaction_state['topic']->topic_id)) {
    $entity->content['satisfaction'] = array(
      '#markup' => theme('satisfaction_topic_replies', array('topic_id' => $entity->satisfaction_state['topic']->topic_id)),
    );
  }
}

/**
 * Implements hook_oauthconnector_presets_alter().
 *
 * Describe an additional oauth_connector for Get Satisfaction.
 */
function satisfaction_oauthconnector_presets_alter(&$presets) {

  $preset = new stdClass();
  $preset->name = '';
  $preset->title = 'Get Satisfaction';
  $preset->url = 'https://getsatisfaction.com';
  $preset->consumer_advanced = array(
    'oauth2' => 0,
    'signature method' => 'HMAC-SHA1',
    'authentication realm' => '',
    'request token endpoint' => '/api/request_token',
    'authorization scope' => '',
    'authorization endpoint' => '/api/authorize',
    'access token endpoint' => '/api/access_token',
  );
  $preset->mapping = array(
    'fields' => array(
      'uid' => array(
        'resource' => 'https://api.getsatisfaction.com/me',
        'method post' => 0,
        'field' => 'id',
        'querypath' => FALSE,
        'sync_with_field' => '',
      ),
      'name' => array(
        'resource' => 'https://api.getsatisfaction.com/me',
        'method post' => 0,
        'field' => 'name',
        'querypath' => FALSE,
        'sync_with_field' => '',
      ),
      'avatar' => array(
        'resource' => 'https://api.getsatisfaction.com/me',
        'method post' => 0,
        'field' => 'avatar',
        'querypath' => FALSE,
        'sync_with_field' => '',
      ),
    ),
    'format' => 'json',
  );

  $presets['satisfaction'] = array(
    'data' => $preset,
    'name' => 'Get Satisfaction'
  );
}

// Helpers
// -----------------------------------------------------------------------------

/**
 * Checks if the user has configured a Get Satisfaction connection.
 *
 * @return bool
 *  TRUE if the user has Get Satisfaction OAuth connection, FALSE otherwise.
 */
function satisfaction_is_user_connected() {
  global $user;

  $count = db_select('authmap', 'a')
    ->fields('a', array('authname'))
    ->condition('uid', $user->uid)
    ->condition('authname', '%' . db_like('get_satisfaction') . '%', 'LIKE')
    ->countQuery()
    ->execute()
    ->fetchField();

  if ($count) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Returns a list of bundle/entity pairs enabled to integrate with Get Satisfaction.
 *
 * @return array
 *  A list of enabled bundles keyed by their entity types.
 */
function satisfaction_get_enabled_entities() {
  $entities = entity_get_info();
  $enabled_entities = array();

  foreach ($entities as $entity_key => $entity) {
    if (!$entity['fieldable']) {
      continue;
    }

    $enabled_entities[$entity_key] = array();

    foreach ($entity['bundles'] as $bundle_key => $bundle) {
      $field_key = 'satisfaction_entity_' . $entity_key . '_' . $bundle_key;

      if (variable_get($field_key, FALSE)) {
        $enabled_entities[$entity_key][] = $bundle_key;
      }
    }
  }

  return $enabled_entities;
}

/**
 * Checks wether a given entity/bundle pair is enabled to use satisfaction
 * integration.
 *
 * @param stdClass $entity
 *  The entity to be check.
 * @param string $entity_type
 *  The entity type machine-name.
 * @return bool
 *  TRUE if the entity uses satisfaction, FALSE otherwise.
 */
function satisfaction_is_enabled_entity($entity, $entity_type) {
  $entity_info = entity_extract_ids($entity_type, $entity);
  $entity_id = $entity_info[0];
  $entity_bundle = $entity_info[2];

  $field_key = 'satisfaction_entity_' . $entity_type . '_' . $entity_bundle;

  return variable_get($field_key, FALSE);
}

/**
 * Generates a string composed of a title and an id.
 *
 * @param string $title
 *  The key title.
 * @param string $id
 *  The key ID.
 * @return string
 *  The generated key string.
 *
 * @see satisfaction_key_string_decompose()
 */
function satisfaction_key_string_compose($title, $id) {
  return '"' . $title . '" (' . $id . ')';
}

/**
 * Decomposes a key string into its parts.
 *
 * @param string $key_string
 *  The key string.
 * @return array
 *  An array with the following keys:
 *  - title: The key title
 *  - id: The key id.
 *
 * @see satisfaction_key_string_compose()
 */
function satisfaction_key_string_decompose($key_string) {
  $key = array();
  $pattern = '/^"(.+)"\s\(([0-9]+)\)$/';
  $matches = array();

  $result = preg_match($pattern, $key_string, $matches);

  if ($result) {
    $key['title'] = $matches[1];
    $key['id'] = $matches[2];
  }

  return $key;
}
